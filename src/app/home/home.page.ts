import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ModalplacePage } from '../modalplace/modalplace.page';
import { OpentripmapService } from '../services/opentripmap.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  placesFound: any[];
  cityFound: any;
  countPlacesFound: any;
  searchPlaceForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private opentripmapService: OpentripmapService,
    private modalController: ModalController
  ) {
    this.searchPlaceForm = this.formBuilder.group({
      txtPlace: new FormControl(
        environment.openTripMap.defaultCity,
        Validators.compose([Validators.required, Validators.minLength(2)])
      ),
    });
  }
  async ionViewDidEnter() {
    await this.loadPageData(environment.openTripMap.defaultCity);
  }

  async searchPlace(formSearch: any) {
    await this.loadPageData(formSearch.txtPlace);
  }

  async loadPageData(place: string) {
    this.cityFound = null;
    this.countPlacesFound = null;
    this.placesFound = null;
    try {
      this.cityFound = await this.opentripmapService.getCityFromSearch(place);
      if (this.cityFound) {
        this.countPlacesFound = await this.opentripmapService.getPlacesByLatLon(
          environment.openTripMap.radius,
          this.cityFound.lon,
          this.cityFound.lat,
          environment.openTripMap.rate,
          environment.openTripMap.formatCount
        );
        this.placesFound = await this.opentripmapService.getPlacesByLatLon(
          environment.openTripMap.radius,
          this.cityFound.lon,
          this.cityFound.lat,
          environment.openTripMap.rate,
          environment.openTripMap.formatJson
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  async showDetailPlace(place: any) {
    const placeSelected = await this.opentripmapService.getDetailPlaceByXid(
      place.xid
    );
    const modal = await this.modalController.create({
      component: ModalplacePage,
      componentProps: {
        placeSelected: placeSelected,
      },
    });
    return await modal.present();
  }
}
